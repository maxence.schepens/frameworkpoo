<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('/courselist', [CourseController::class, 'index'])->name('courselist');


Route::get('/register', [UserController::class, 'create'])->name('register');
Route::post('/register', [UserController::class, 'store']);

Route::get('/validate', function () {
    return view('validate');
})->name('validate');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');


Route::get('/login', [LoginController::class, 'loginView'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate'])->name('loginAuth');

Route::get('/profile/{user}', [UserController::class, 'show'])->name('profile');




//Route::middleware(['auth'])->group(function () {
//    Route::get('/profile/{id}', [UserController::class, 'show'])->name('profile');
//});
//Route::post('/test', [UserController::class, 'test'])->name('test');

