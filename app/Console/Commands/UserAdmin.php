<?php

namespace App\Console\Commands;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Validation\Validator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $isValid = false;
        while (!$isValid) {
            $username = $this->ask('Administrator name :(min:4)');
            $password = $this->ask('Administrator password :(min:8)');
            $email = $this->ask('Administrator email :');

            $username = strtolower($username);

            $check = Validator::make([
                'username' => $username,
                'password' => $password,
                'email' => $email
            ], [
                'username' => 'required|string|min:4|unique:user',
                'password' => 'required|string|min:8',
                'email' => 'required|string|email',

                ]);
        }
    }
}
