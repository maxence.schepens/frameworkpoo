<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('register');
    }

//    public function test(Request $request): Response
//    {
//        dd($request);
//    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $utilisateurRoles = Roles::where('name', 'utilisateur')->first();

        if (!$utilisateurRoles) {
            $utilisateurRoles = Roles::create(['name' => 'utilisateur']);
        }

        $user = $request->validated();
        DB::table('user')->insert([
            'username' => $user['username'],
            'password' => password_hash($user['password'], PASSWORD_BCRYPT),
            'email' => $user['email'],
            'created' => now(),
            'lastlogin' => now(),
            'role_id' => $utilisateurRoles->id
        ]);

        // Redirection vers la route pour accéder a la vue Validate, puis utilisation du service de gestion d'erreur pour le message
        return redirect()->route('validate')->with('success', 'Inscription réussie');
    }

    /**
     * Display the specified resource.
     */

    public function show(User $user)
    {
        if (!is_null($user)) {
            // Récupérez l'utilisateur connecté
            $auth = $user;
            // Vérifiez si l'ID passé en paramètre correspond à l'ID de l'utilisateur connecté
            if ($auth/*->attributes*/->id == $user->id) {
                // Récupérez les informations du profil de l'utilisateur à partir de la base de données
                $result = DB::table('user')
                    ->select('username', 'email', 'created', 'lastlogin', 'roles.name as Role')
                    ->join('roles', 'role_id', '=', 'roles.id')
                    ->where("user.id",$user->id)
                    ->first();
                    //->find($user->id)
                // Affichez la vue du profil avec les informations récupérées
                return view('profile', ['profiles' => $result]);
            } else {
                // L'ID ne correspond pas à l'utilisateur connecté, redirigez vers la page de profil correspondant ou renvoyez une erreur
                return redirect()->intended('profile', ['id' => $auth->attributes->id]);
            }
        } else {
            // L'utilisateur n'est pas connecté, redirigez-le vers la page de connexion
            return redirect('/login')->with('error', 'Connexion échouer');
        }
    }
//    public function show($id)
//{
//    if (Auth::check()){
//        $user = Auth::user();
//        $result = DB::table('user')
//            ->select('username', 'email', 'created', 'lastlogin', 'role_id')
//            ->find($user);
//        return view('profile', ['profiles' => $result]);
//    }
//

    // Vérifier si l'utilisateur est connecté
//    if (Auth::check()) {
//        // Récupérer l'utilisateur à partir de l'ID
//        $user = User::find($id);
//        // Vérifier si l'utilisateur existe
//        if ($user) {
//            // Afficher le profil de l'utilisateur
//            $roleName = Roles::find($user->role_id)->name;
//            return view('profile', ['user' => $user]);
//        } else {
//            // L'utilisateur n'existe pas, redirigez vers la page d'erreur correspondante
//            $roleName = 'utilisateur';
//            return redirect()->route('error.404');
//        }
//    } else {
//        // L'utilisateur n'est pas connecté, redirigez-le vers la page de connexion
//        return redirect('/login');
//    }
//}


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }

    public static function updateLastLogin(int $userId)
    {
        DB::table('user')->where('id', '=', $userId)
            ->update([
                'lastlogin' => now()
            ]);
    }
}
