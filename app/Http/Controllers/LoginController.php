<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    // Gérer la redirection de la vue login
    public function loginView()
    {
        return view('login');
    }


//     Gérer une tentative de connexion
    public function authenticate(Request $request): RedirectResponse
    {
        $result = $request->validate([
            'username' => ['required'],
            'password' => ['required']
        ]);

        if (Auth::attempt($result)) {
            $request->session()->regenerate();
            $user = Auth::user();
            session(['user_id' => $user->id, 'admin' => $user->role_id]);
            UserController::updateLastLogin($user->getAuthIdentifier());
            return redirect()->route('profile' , $user->getAuthIdentifier());
        }

        return back()->withErrors([
            'msg' => 'Les informations rentrées sont fausses !'
        ])->onlyInput('username');
    }


    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/')->with('success', 'Aurevoir !');
    }
}
