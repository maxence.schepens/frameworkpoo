@extends('layouts.body')

@section('title', 'Liste des cours')

@section('content')
    <h2>Liste de cours</h2>
    <table class="table table-bordered">
        <thead class="table-dark">
        <tr>
            <th>Nom du cours</th>
            <th class="text-end">Code</th>
        </tr>
        </thead>
        <tbody class="table-secondary">
        @foreach($courses as $course)
            <tr>
                <td>{{ $course->name }}</td>
                <td class="text-end">{{ $course->code }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
