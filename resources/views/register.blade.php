@extends('layouts.body')

@section('title', 'Formulaire d\'inscription')

@section('content')
    <div class="container">
        <h2>Inscription</h2>
        <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
            @csrf
            <label class="form-label" for="username">Username</label>
            <input class="form-control" type="text" id="username" name="username"><br>
            <label class="form-label" for="password">Mot de passe</label>
            <input class="form-control" type="password" id="password" name="password"><br>
            <label class="form-label" for="email">Email</label>
            <input class="form-control" type="email" id="email" name="email"><br>
{{--            <label class="form-label" for="photo">Photo de profil</label>--}}
{{--            <input class="form-control" type="file" id="photo" name="photo" accept="image/jpeg, image/png"><br>--}}
            <button class="btn btn-primary">Envoyez</button>
        </form>
    </div>
@endsection
@error('username')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
@error('password')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
@error('email')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
