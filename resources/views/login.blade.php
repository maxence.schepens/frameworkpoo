@extends('layouts.body')

@section('title', 'Formulaire de connexion')

@section('content')
    <div class="container">
        <h2>Connexion</h2>
        <form action="{{ route('loginAuth') }}" method="post">
            @csrf
            <label class="form-label" for="username">Identifiant</label>
            <input class="form-control" type="text" id="username" name="username" required><br>
            <label class="form-label" for="password">Mot de passe</label>
            <input class="form-control" type="password" id="password" name="password" required><br>
            <a href="{{ route('register') }}">Pas encore inscrit ?</a>
            <button class="btn btn-primary" type="submit">Connexion</button>
        </form>
    </div>

@endsection
@if(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif
@if(session('errors'))
    <div class="alert alert-danger">{{ session('errors')->first('msg') }}</div>
@endif
@if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
@endif
