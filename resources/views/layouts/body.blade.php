<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/webex.css')  }}">
    <title>@yield('title')</title>
</head>
<body>
<nav class="navbar navbar-expand-lg border border-black">
    <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="{{ route('index') }}">Accueil</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link active" aria-current="page" href="{{ route('courselist') }}">Liste des cours</a>
                @if(auth()->check())
                    <a class="nav-link" href="{{ route('profile', [session('user_id')]) }}">Profil</a>
                    <a href="{{ route('logout') }}" class="nav-link">Logout</a>
                    @if(session('admin'))
                        <a href="" class="nav-link">Admin</a>
                    @endif
                @else
                    <a href="{{ route('login') }}" class="nav-link">Login</a>
                @endif
            </div>
        </div>
    </div>
</nav>
<main>
    @yield('content')
</main>
<footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
            crossorigin="anonymous"></script>
</footer>
</body>
</html>
