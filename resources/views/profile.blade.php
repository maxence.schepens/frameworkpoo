@extends('layouts.body')

@section('title', 'Profil d\'utilisateur')

@section('content')
    <div class="container text-center">
        <h2>Profil d'utilisateur</h2>
        <table class="table table-bordered">
            <thead class="table-dark">
            <tr>
                <th scope="col">Intitulé</th>
                <th scope="col">Valeur</th>
            </tr>
            </thead>
            <tbody class="table-secondary">
            @foreach($profiles as $key => $value)
                <tr>
                    <td>{{ ucfirst($key) }}</td>
                    <td class="">{{ $value }}</td>
                </tr>
            @endforeach
            {{--            <tbody>--}}
            {{--            <tr>--}}
            {{--                <th>Identifiant :</th>--}}
            {{--                <td> {{ $user->email }}</td>--}}
            {{--            </tr>--}}
            </tbody>
        </table>
    </div>
@endsection

