<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 255)->unique()->index();
            $table->string('password', 255);
            $table->string('email', 255)->unique()->index();
            $table->dateTime('created');
            $table->dateTime('lastLogin')->nullable();
            $table->unsignedBigInteger('role_id')->default(1)->nullable(false);
            $table->string('image')->nullable();
            $table->foreign('role_id')->references('id')->on('Roles');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user');
    }
};
