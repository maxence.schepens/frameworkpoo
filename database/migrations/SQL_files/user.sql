-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 09 oct. 2023 à 09:50
-- Version du serveur : 8.0.31
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `webex`
--

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `created`, `lastlogin`, `role_id`, `image`) VALUES
(10, 'test02', '$2y$10$D7CzRO8uGCus6W0NYjoxHu47xWzo0B11Bo0VIk4tDR8InOjYncuhO', 'test02@gmail.com', '2023-06-15 11:20:31', '2023-06-21 16:40:00', 0, NULL),
(11, 'admin', '$2y$10$0PS0l4T4EAk5mDxf7hzYB.qY6omBthobmifhZFIgFNASPHbex.QzW', 'admin@gmail.com', '2023-06-21 11:51:27', '2023-06-28 23:19:41', 0, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
